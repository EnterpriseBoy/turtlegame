﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using TurtleGame.Model;

namespace TurtleGame
{
    public class XmlParser
    {
        public Settings ParseGame(string xmlFile)
        {
            var mySettings = new Settings();

            var xmlDoc = new XmlDocument(); 
            xmlDoc.LoadXml(xmlFile);
            mySettings.GameHeight = int.Parse(xmlDoc.GetElementsByTagName(Constants.Height)[0].InnerText);
			mySettings.GameWidth = int.Parse(xmlDoc.GetElementsByTagName(Constants.Width)[0].InnerText);
			mySettings.ExitPosX = int.Parse(xmlDoc.GetElementsByTagName(Constants.ExitPosX)[0].InnerText);
			mySettings.ExitPosY = int.Parse(xmlDoc.GetElementsByTagName(Constants.ExitPosY)[0].InnerText);
			mySettings.UserPosX = int.Parse(xmlDoc.GetElementsByTagName("startposx")[0].InnerText);
			mySettings.UserPosY = int.Parse(xmlDoc.GetElementsByTagName("startposy")[0].InnerText);

			return mySettings;
        }

		public List<Location> ParseLocation(XElement xmlDocument)
		{
			var listLocations = new List<Location>();

			foreach (XElement node in xmlDocument.Nodes())
			{
				var mine = new Location();
				mine.posx = Int32.Parse( node.Element(Constants.MinesPosX).Value);
				mine.posy = Int32.Parse(node.Element(Constants.MinesPosY).Value);
				listLocations.Add(mine);
			}
			return listLocations;
		}

		public List<string> ParseMove(XElement xmlDocument)
		{
			var ListMoves = new List<string>();
			foreach (XElement node in xmlDocument.Nodes())
			{
				ListMoves.Add(node.Value);
			}
			return ListMoves;
		}
	}
}
