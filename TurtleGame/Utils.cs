﻿using log4net;
using System;
using System.IO;
using TurtleGame.Model;

namespace TurtleGame
{
    public static class Utils
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        public static string ReadCommandArgs(string input,bool argLocation)
        {
            var errorMsg = string.Empty;

            if (string.IsNullOrEmpty(input))
                log.Error(string.Format(Constants.FileErrorMessageNoValuePassed, input));

            if (input.IndexOf(":") == -1)
                log.Error(Constants.FileErrorMessageNoLocation);


            var colonLocation = input.IndexOf(':');

            if(colonLocation == input.Length-1)
                log.Error(Constants.FileErrorMessageNoArgument);

            if (argLocation)
                return input.Substring(0,colonLocation);

            return input.Substring(colonLocation + 1, (input.Length-1)- colonLocation);
        }

        public static void DrawGame(int width,int height)
        {

            var box = new Draw();
            for (int l = 0; l < width; l++)
            {
                for (int i = 0; i < 3; i++)
                {
                    box.TopLine();
                }

                Console.WriteLine();
                for (int i = 0; i < height; i++)
                {
                    box.Middle();
                }
                Console.WriteLine();
                for (int i = 0; i < 3; i++)
                {
                    box.TopLine();
                }
                Console.WriteLine();
            }
        }

        public static string ReadFile(string location)
        {
            string result = null;
            using (StreamReader reader = new StreamReader(location))
            {
                result = reader.ReadToEnd();
            }

            return result;
        }

		public static Location Move(string move, Location location,int gameWidth, int gameHeight)
		{
			//Location result = null;

			switch (move.ToLower())
			{
				case Constants.Left:
					if ((location.posx - 1) > 0)
						location = new Location { posx = (location.posx - 1), posy = location.posy };
					break;

				case Constants.right:
					if ((location.posx + 1) <= gameWidth)
						location = new Location { posx = (location.posx + 1), posy = location.posy };
					break;

				case Constants.up:
					if ((location.posy + 1) <= gameHeight)
						location = new Location { posx = location.posx, posy = (location.posy + 1) };
					break;

				case Constants.down:
					if ((location.posy - 1) > 0)
						location = new Location { posx = location.posx, posy = (location.posy -1) };
					break;
			}

			return location;
		}
    }
}
