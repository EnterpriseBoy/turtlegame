﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurtleGame.Model
{
	public class Location
	{
		public int posx { get; set; }
		public int posy { get; set; }
	}
}
