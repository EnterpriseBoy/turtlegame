﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurtleGame.Model
{
    public class Settings
    {
        public int GameWidth { get;  set; }
        public int GameHeight { get;  set; }
		public int UserPosX { get; set; }
		public int UserPosY { get; set; }
		public int ExitPosX { get; set; }
		public int ExitPosY { get; set; }
		public string username { get; set; }
        public DateTime startdate { get; set; }
    }
}
