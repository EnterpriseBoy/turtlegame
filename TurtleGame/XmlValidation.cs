﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace TurtleGame
{
    public class XmlValidation
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        private string _settings = null;
        private string _moves = null;

        public XmlValidation(string gameInstructions,string usersMoves)
        {
            _settings = gameInstructions;
            _moves = usersMoves;
        }


        private XElement ValidateXmlFileComplete(string input)
        {
            try {
                return XElement.Parse(input);
            }catch(Exception ex)
            {
                log.Error(Constants.XmlNotPassedIn);
                log.Error(ex.Message);
                return null;
            }
        }

        private bool ValidateNodeExists(XElement xmlFile, List<string> nodes)
        {
            var result = true;
            foreach(var node in nodes)
            {
				if (xmlFile.Descendants(node).Count() <=0 || xmlFile.Descendants(node).Count() > 1)
                {
                    result = false;
                    log.Error(string.Format("Incorrect number of {0} nodes found in file",node));
                }
            }
            return result;
        }


		private bool ValidateNodeValue(XElement xmlFile, List<string> nodes)
		{
			var result = true;
			foreach (var node in nodes)
			{
				if (Int32.TryParse(xmlFile.Descendants(node).Single().Value, out _))
					return true;

				log.Error(Constants.XmlValueNotNumber);
				return false;
			}
			return result;
		}

		private bool ValidateMines(XElement xmlDocument)
		{
			var result = true;

			if (xmlDocument.Descendants(Constants.Mine).Count() <= 0 )
			{
				result = false;
				log.Error(string.Format("No mines definations, game must have at least one mine", Constants.Mine));
			}

			foreach (XElement node in xmlDocument.Nodes())
			{
				if (node.Descendants(Constants.MinesPosX).Count() != node.Descendants(Constants.MinesPosY).Count())
				{
					log.Error("Miss match in mine position values");
					result = false;
				}
			}

			return result;
		}

        public bool ValidateSettings()
        {
            var xmlDocument = ValidateXmlFileComplete(_settings);

			var nodeMainSettings = new List<string>(new string[]{"gamesettings","usersettings","mines"});
			var nodeGameSettings = new List<string>(new string[] { "width", "height", "lives" });
			var nodeUserSettings = new List<string>(new string[] { "startposx", "startposy" });
			var nodeMines = new List<string>(new string[] { "mine"});


			var result = true;
			if (xmlDocument == null)
			{
				log.Error("Empty file passed in");
				return false;
			}
            else
            {
				if (!ValidateNodeExists(xmlDocument, nodeMainSettings))
				{
					log.Error(string.Format(Constants.XmlErrorForSection, "main"));
					result = false;
				}

				//Gamesetting validation
				var xmlDoc = xmlDocument.Element("gamesettings") as XElement;
				if (!ValidateNodeExists(xmlDoc, nodeGameSettings))
				{
					log.Error(string.Format(Constants.XmlErrorForSection, "game"));
					result = false;
				}

				if (!ValidateNodeValue(xmlDoc, nodeGameSettings))
				{
					log.Error(string.Format(Constants.XmlErrorForSection, "game"));
					result = false;
				}

				//Usersetting validation
				xmlDoc = xmlDocument.Element("usersettings") as XElement;
				if (!ValidateNodeExists(xmlDoc, nodeUserSettings))
				{
					log.Error(string.Format(Constants.XmlErrorForSection, "user"));
					result = false;
				}

				if (!ValidateNodeValue(xmlDoc, nodeUserSettings))
				{
					log.Error(string.Format(Constants.XmlErrorForSection, "game"));
					result = false;
				}

				//Mines validation
				xmlDoc = xmlDocument.Element("mines") as XElement;

				if (!ValidateMines(xmlDoc))
				{
					Console.WriteLine("Mine validation failed see logfile for more information");
					result = false;
				}
            }
            return result;
        }

		public bool ValidateMoves()
		{
			var result = true;
			var xmlDocument = ValidateXmlFileComplete(_moves);


			if (xmlDocument == null)
			{
				log.Error("Empty file passed in");
				return false;
			}


			if (xmlDocument.Descendants(Constants.Move).Count() <= 0)
			{
				result = false;
				log.Error(string.Format("No moves defined, game must have at least one moves", Constants.Mine));
			}
			return result;
		}

	}
}
