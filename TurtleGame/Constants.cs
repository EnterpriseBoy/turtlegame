﻿namespace TurtleGame
{
    public static class Constants
    {
        //Settings
        public const string Width = "width";
        public const string Height = "height";
		public const string ExitPosX = "exitposx";
		public const string ExitPosY = "exitposy";
		public const string Mines = "mines";
		public const string Mine = "mine";
		public const string Move = "move";
		public const string MinesPosX = "mineposx";
		public const string MinesPosY = "mineposy";

		//Moves
		public const string Left = "left";
		public const string right = "right";
		public const string up = "up";
		public const string down = "down";


		public const string FileErrorMessageNoLocation = "Command line argument did not have a location";
        public const string FileErrorMessageNoArgument = "No argument value was passed in for {0}";
        public const string FileErrorMessageNoValuePassed = "The value of {0} was blank";
        public const string FileExistsError = "File does {0} not exist or you do not have permission to access it";

        //Exit messsages
        public const string PressKeyExit = "Press a key to exit game";

        //Command line Arguments
        public const string GameInstructionsLocations = "gameinstructionlocation";
        public const string UsersMovesLocation = "usersmoveslocation";
        public const string IncorrectCommandLine = "Incorrect command line argument entered";

        //Xml Validation Errors
        public const string XmlValueIncorrect = "The number of values of {0} was incorrect";
        public const string XmlNotPassedIn = "A non valid xml file was passed in";
        public const string XmlValueNotNumber = "A non numeric value was passed in";
        public const string XmlCountExceeded = "The number of {0} in the XML file was {1} and should have been {2}";
		public const string XmlErrorForSection = "Validation failed for {0} settings, check the log files";

	}
}
