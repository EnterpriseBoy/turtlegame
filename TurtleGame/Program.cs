﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using TurtleGame.Model;

namespace TurtleGame
{
    class Program
    {
        static void Main(string[] args)
        {
            string settings = null;
            string usersMovesFileLocation = null;
            string gameSettignsfile = null;
            string usersMovesFile = null;
			XElement xmlMines = null;
			XElement xmlMoves = null;
			var gameSettings = new Settings();

            var hasErrors = false;

            if (args.Length == 0)
            {
                Console.WriteLine("You did not enter any file details.");
                Console.WriteLine(Constants.PressKeyExit);
                Console.ReadKey();
                Environment.Exit(0);
            }

            foreach (var commandArg in args)
            {
                switch(Utils.ReadCommandArgs(commandArg, true).ToLower())
                {
                    case Constants.GameInstructionsLocations:
                        settings = Utils.ReadCommandArgs(commandArg, false);
                        break;

                    case Constants.UsersMovesLocation:
                        usersMovesFileLocation = Utils.ReadCommandArgs(commandArg, false);
                        break;

                    default:
                        hasErrors = true;
                        break;
                }
            }
            if (hasErrors)
            {
                Console.WriteLine(Constants.IncorrectCommandLine);
                Environment.Exit(0);
            }

           if (string.IsNullOrEmpty(settings) || string.IsNullOrEmpty(usersMovesFileLocation))
            {
                Console.WriteLine(string.Format(Constants.FileErrorMessageNoArgument,"Gamefile or Users moves"));
                Console.ReadKey();
                Environment.Exit(0);
            }

           //Check files exist
            if (!File.Exists(settings))
                Console.WriteLine(string.Format(Constants.FileExistsError,"game instructions"));

            if (!File.Exists(usersMovesFileLocation))
                Console.WriteLine(string.Format(Constants.FileExistsError, "UserMoves"));


            //Read the files
            gameSettignsfile = Utils.ReadFile(settings);
            usersMovesFile = Utils.ReadFile(usersMovesFileLocation);

            //XML Validation
            var xmlValidation = new XmlValidation(gameSettignsfile, usersMovesFile);
            if (!xmlValidation.ValidateSettings())
            {
                Console.WriteLine("Validation Failed for settings. Check the log file for error");
                Console.ReadKey();
            }

			if (!xmlValidation.ValidateMoves())
			{
				Console.WriteLine("Validation Failed for moves. Check the log file for error");
				Console.ReadKey();
			}

			gameSettings = new XmlParser().ParseGame(gameSettignsfile);

			Console.WriteLine("Game height: "+gameSettings.GameHeight);
            Console.WriteLine("Game width: " + gameSettings.GameWidth);
			Console.WriteLine("User position X: " + gameSettings.UserPosX);
			Console.WriteLine("User positino Y: " + gameSettings.UserPosY);
			Console.WriteLine("Exit position X: " + gameSettings.ExitPosX);
			Console.WriteLine("Exit positino Y: " + gameSettings.ExitPosY);

			var xmlParser = new XmlParser();

			xmlMines = XElement.Parse(gameSettignsfile).Element("mines") as XElement;
			var mines = new List<Location>();
			foreach (var mine in xmlParser.ParseLocation(xmlMines))
			{
				mines.Add(new Location { posx = mine.posx, posy = mine.posy });
			}

			xmlMoves = XElement.Parse(usersMovesFile) as XElement;

			var userInitalPosition = new Location { posx = gameSettings.UserPosX, posy = gameSettings.UserPosY };
			var exitPosition = new Location { posx = gameSettings.ExitPosX, posy = gameSettings.ExitPosY };

			Console.WriteLine($"Starting Location is posx: {gameSettings.UserPosX}, posy: {gameSettings.UserPosY}");

			Console.WriteLine("Move positions");
			Console.ReadKey();

			var UserPosition = new Location { posx = gameSettings.UserPosX, posy =gameSettings.UserPosY };

			foreach (var move in xmlParser.ParseMove(xmlMoves))
			{
				var tempLocation = new Location{posx = UserPosition.posx,posy = UserPosition.posy };

				UserPosition = Utils.Move(move, UserPosition, gameSettings.GameHeight, gameSettings.GameWidth);
				Console.WriteLine($"You moved : {move}");

				if (tempLocation.posx == UserPosition.posx && tempLocation.posy == UserPosition.posy)
					Console.WriteLine("Move failed as it was outside the bounds of the game");

				Console.WriteLine($"Current Location is posx: {UserPosition.posx}, posy: {UserPosition.posy}");
				Console.ReadKey();

				if (mines.Any(x => x.posy == UserPosition.posy && x.posx == UserPosition.posx))
				{
					Console.WriteLine("Sorry you stood on a mine!!");
					Console.WriteLine("Press any key to play again");
					Console.ReadKey();
					Environment.Exit(0);
				}

				if (gameSettings.ExitPosX == UserPosition.posx && gameSettings.ExitPosY == UserPosition.posy)
				{
					Console.WriteLine("Congratulations you escaped well done");
					Console.ReadKey();
					Environment.Exit(0);
				}

			}
			Console.WriteLine("I am so sorry but you did not escape!");
			Environment.Exit(0);
			Console.ReadLine();
        }


    }
}
