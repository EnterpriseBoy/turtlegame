﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TurtleGame;

namespace TurtleTests
{
	[TestClass]
	public class ValidationGameTest
	{
		[TestMethod]
		public void ValidationTest_Moves_NoError()
		{
			var xmlSetting =
						@"
                        <moves>
							<move>left</move>
							<move>left</move>
							<move>left</move>
							<move>left</move>
							<move>left</move>
							<move>left</move>
							<move>right</move>
						</moves>
                        ";

			Assert.IsTrue(new XmlValidation(null, xmlSetting).ValidateMoves());
		}

		[TestMethod]
		public void ValidationTest_MovesBlank_Error()
		{
			var xmlSetting =
						@"
                        <moves>
						</moves>
                        ";

			Assert.IsFalse(new XmlValidation(null, xmlSetting).ValidateMoves());
		}

		[TestMethod]
		public void ValidationTest_MovesBadXml_NoError()
		{
			var xmlSetting =
						@"
                        <moves1>
							<move>left</move>
							<move>left</move>
							<move>left</move>
							<move>left</move>
							<move>left</move>
							<move>left</move>
							<move>right</move>
						</moves>
                        ";

			Assert.IsFalse(new XmlValidation(null, xmlSetting).ValidateMoves());
		}
	}
}
