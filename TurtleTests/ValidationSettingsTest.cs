﻿using TurtleGame;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TurtleTests
{
    [TestClass]
    public class ValidationSettingsTest
    {
        [TestMethod]
        public void ValidationTest_GameSettings_NoError()
        {
            var xmlSetting =
						@"
                        <game>
	                        <gamesettings>
		                        <width>5</width>
		                        <height>10</height>
		                        <lives>3</lives>
	                        </gamesettings>
	                        <usersettings>
		                        <startposx>2</startposx>
		                        <startposy>2</startposy>
	                        </usersettings>
	                        <mines>
								<mine>
									<mineposx>2</mineposx>
									<mineposy>2</mineposy>
								</mine>
								<mine>
									<mineposx>2</mineposx>
									<mineposy>2</mineposy>
								</mine>
	                        </mines>
                        </game>
                        ";

            var test = new XmlValidation(xmlSetting,null );

            Assert.IsTrue(test.ValidateSettings());
        }

		[TestMethod]
        public void ValidationTest_GameSettingsBadWidth_Error()
        {
			var xmlSetting =
						@"
                        <game>
	                        <gamesettings>
		                        <width>FAIL</width>
		                        <height>10</height>
		                        <lives>3</lives>
	                        </gamesettings>
	                        <usersettings>
		                        <startposx>2</startposx>
		                        <startposy>2</startposy>
	                        </usersettings>
	                        <mines>
		                        <minexposx>2</minexposx>
		                        <mineyposy>2</mineyposy>

		                        <minexpos>2</minexpos>
		                        <mineypos>3</mineypos>
	
		                        <minexpos>2</minexpos>
		                        <mineypos>4</mineypos>
	                        </mines>
                        </game>
                        ";

            Assert.IsFalse(new XmlValidation(xmlSetting, null).ValidateSettings());
        }

        [TestMethod]
        public void ValidationTest_GameSettingsHeightTwice_Error()
        {
			var xmlSetting =
						@"
                        <game>
	                        <gamesettings>
		                        <width>5</width>
		                        <height>10</height>
		                        <height>10</height>
		                        <lives>3</lives>
	                        </gamesettings>
	                        <usersettings>
		                        <startposx>2</startposx>
		                        <startposy>2</startposy>
	                        </usersettings>
	                        <mines>
		                        <minexposx>2</minexposx>
		                        <mineyposy>2</mineyposy>

		                        <minexpos>2</minexpos>
		                        <mineypos>3</mineypos>
	
		                        <minexpos>2</minexpos>
		                        <mineypos>4</mineypos>
	                        </mines>
                        </game>
                        ";
			var test = new XmlValidation(xmlSetting, null);

            Assert.IsFalse(test.ValidateSettings());
        }

        [TestMethod]
        public void ValidationTest_GameSettingsBadXml_Error()
        {
			var xmlSetting =
						@"
                        <game>
	                        <gamesettings>
		                        <width22>5</width>
		                        <height>10</height>
		                        <height>10</height>
		                        <lives>3</lives>
	                        </gamesettings>
	                        <usersettings>
		                        <startposx>2</startposx>
		                        <startposy>2</startposy>
	                        </usersettings>
	                        <mines>
		                        <minexposx>2</minexposx>
		                        <mineyposy>2</mineyposy>

		                        <minexpos>2</minexpos>
		                        <mineypos>3</mineypos>
	
		                        <minexpos>2</minexpos>
		                        <mineypos>4</mineypos>
	                        </mines>
                        </game>
                        ";
			var test = new XmlValidation(xmlSetting, null);

            Assert.IsFalse(test.ValidateSettings());
        }

        [TestMethod]
        public void ValidationTest_GameSettingsBadXmlValue_Error()
        {
			var xmlSetting =
						@"
                        <game>
	                        <gamesettings>
		                        <width>FAIL</width>
		                        <height>10</height>
		                        <height>10</height>
		                        <lives>3</lives>
	                        </gamesettings>
	                        <usersettings>
		                        <startposx>2</startposx>
		                        <startposy>2</startposy>
	                        </usersettings>
	                        <mines>
		                        <minexposx>2</minexposx>
		                        <mineyposy>2</mineyposy>

		                        <minexpos>2</minexpos>
		                        <mineypos>3</mineypos>
	
		                        <minexpos>2</minexpos>
		                        <mineypos>4</mineypos>
	                        </mines>
                        </game>
                        ";
			var test = new XmlValidation(xmlSetting, null);

            Assert.IsFalse(test.ValidateSettings());
        }
    }
}
